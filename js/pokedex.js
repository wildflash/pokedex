var { Router, Route, Link, browserHistory } = ReactRouter

var Home = React.createClass({
	source: "https://pokeapi.co/api/v2/pokedex/1/",

	getInitialState: function() {
		return {
			pokemonList: []
		};
	},

	componentDidMount: function() {
		this.serverRequest = $.get(this.source, function(result){
			// console.log(result.pokemon_entries);
			this.setState({
				pokemonList: result.pokemon_entries
			});
		}.bind(this));
	},

	componentWillUnmount: function() {
		this.serverRequest.abort();
	},

	render: function() {
		var title = "Pokemon List"
		// console.log(this.state.pokemonList);

		var pokemons = this.state.pokemonList.map(function(listValue){
			// return <li>{listValue.entry_number} {listValue.pokemon_species.name} {listValue.pokemon_species.url}</li>
			return  ( <li key={listValue.entry_number} className="col-xs-12 col-sm-6 col-md-4 pokedex-list-item">
					    <Link to={'/pokemon/'+listValue.pokemon_species.name}>
					      <div className="thumbnail">
					        <div className="caption">
					          <h3>{listValue.entry_number}. {listValue.pokemon_species.name}</h3>
					        </div>
					      </div>
					    </Link>
					  </li>
					)
		}.bind(this));

		return (
			<div className="app col-xs-12">
				<header className="page-header">
					<h1>{title}</h1>
				</header>
				
				<ul className="pokedex-thumb-list">
					{pokemons}
				</ul>
			</div>
		);
	}
});








var Pokemon = React.createClass({
	sourceBase: "https://pokeapi.co/api/v2/pokemon/",

	getInitialState: function() {
		return {
			pokemonImage: null,
			pokemonTypes: [],
			pokemonStats: []
		};
	},

	componentDidMount: function() {
		this.serverRequest = $.get(this.sourceBase+this.props.params.name, function(result){
			console.log(result);
			this.setState({
				pokemonImage: result.sprites.front_default,
				pokemonTypes: result.types,
				pokemonStats: result.stats
			});
		}.bind(this));
	},

	render: function() {
		var title = this.props.params.name;
		//upper case first letter
		title = title.toLowerCase().replace(/\b[a-z]/g, function(letter) {
		    return letter.toUpperCase();
		});

		

		var types = this.state.pokemonTypes.map(function(listValue){
			return  ( <li>{listValue.type.name}</li> )
		}.bind(this));

		var stats = this.state.pokemonStats.map(function(listValue){
			var barClassName;
			if(listValue.base_stat > 60) barClassName = "progress-bar-success";
			else if(listValue.base_stat > 30) barClassName = "progress-bar-warning";
			else barClassName = "progress-bar-danger";

			return  ( <li className="pokemon-stat-list-item">
				        <p>{listValue.stat.name}</p>
						<div className="progress">
						  <div className={"progress-bar " + barClassName} role="progressbar" aria-valuenow={listValue.base_stat} aria-valuemin="0" aria-valuemax="100"
						  style={{width: listValue.base_stat + '%'}}>
						  </div>
						</div>
				      </li> 
				    )
		}.bind(this));

		return (
			<div className="app col-xs-12">
				<header className="page-header">
					<h1>
					  {title}
					  <span>
					    <Link to="/">
					      <button type="submit" className="pull-right btn btn-warning">Back</button>
					    </Link>
					  </span>
					</h1>
				</header>

				<div className="container-fluid">
					<div className="row">
						<div className="col-xs-0 col-md-3 col-sm-4"/>
						<div className="thumbnail col-xs-12 col-md-6 col-sm-4">
							<img src={this.state.pokemonImage} alt="" width="200" height="200"/>
							<h3>Type</h3>
							<ul>
							  {types}
							</ul>
							<h3>Base Stats</h3>
							<ul>
							  {stats}
							</ul>
						</div>
					</div>
				</div>
			</div>
		);
	}
});






ReactDOM.render((
  <Router>
    <Route path="/" component={Home} />
    <Route path="/pokemon/:name" component={Pokemon} />
  </Router>
), document.getElementById('content'));